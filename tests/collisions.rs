use std::f32::consts::PI;

use glam::{Mat4, Vec3};
use glash::{Aabb, Frustum, Intersect, Plane, Relation};

#[test]
fn frustum_box_inside() {
    let aabb = Aabb::min_max(Vec3::splat(-0.5), Vec3::splat(0.5));
    //We know we are looking down the z axis with until 10.0. Thefore the cube should be inside completely
    let mat = Mat4::perspective_lh(90.0f32.to_radians(), 1.0, 0.1, 20.0)
        * Mat4::look_at_lh(Vec3::Z * -10.0, Vec3::ZERO, Vec3::Y);

    let frustum = Frustum::from_matrix4(mat).unwrap();
    let relation = frustum.intersect(&aabb);
    assert!(relation == Relation::In, "Expected In, got {:?}", relation);
}

#[test]
fn frustum_box_cross() {
    let aabb = Aabb::min_max(Vec3::splat(-0.5), Vec3::splat(0.5));
    //We know we are looking down the z axis with until 0.0. Thefore the cube should intersect
    let mat = Mat4::look_at_lh(Vec3::Z * -10.0, Vec3::ZERO, Vec3::Y)
        * Mat4::perspective_lh(90.0f32.to_radians(), 1.0, 0.0, 10.0);

    let frustum = Frustum::from_matrix4(mat).unwrap();
    let relation = frustum.intersect(&aabb);
    assert!(
        relation == Relation::Cross,
        "Expected Cross, got {:?}",
        relation
    );
}

#[test]
fn frustum_box_outside() {
    let aabb = Aabb::min_max(Vec3::splat(-100.0), Vec3::splat(-50.0));
    let mat = Mat4::look_at_lh(Vec3::splat(-10.0), Vec3::ZERO, Vec3::Y)
        * Mat4::perspective_infinite_lh(PI, 1.0, 0.1);

    let frustum = Frustum::from_matrix4(mat).unwrap();
    let relation = frustum.intersect(&aabb);
    assert!(
        relation == Relation::Out,
        "Expected Out, got {:?}",
        relation
    );
}

#[test]
fn frustum_box_outside2() {
    let aabb = Aabb::min_max(Vec3::splat(10.0), Vec3::splat(20.0));
    //We know we are looking down the z axis with until 0.0. Thefore the cube should be outside
    let mat = Mat4::look_at_lh(Vec3::Z * -10.0, Vec3::ZERO, Vec3::Y)
        * Mat4::perspective_lh(PI, 1.0, 0.0, 10.0);

    let frustum = Frustum::from_matrix4(mat).unwrap();
    let relation = frustum.intersect(&aabb);
    assert!(
        relation == Relation::Out,
        "Expected Out, got {:?}",
        relation
    );
}

#[test]
fn frustum_plane_inside() {
    let _plane = Plane::new(Vec3::Y, 0.0);
}

#[test]
fn frustum_plane_cross() {
    assert_eq!(3 + 2, 5);
}
#[test]
fn frustum_plane_outside() {
    assert_eq!(3 + 2, 5);
}
