<div align="center">

# Glash

Simple collision primitive library based on [glam](https://crates.io/crates/glam) inspired by the [collision](https://crates.io/crates/collision).


[![dependency status](https://deps.rs/repo/gitlab/tendsinmende/glash/status.svg)](https://deps.rs/repo/gitlab/tendsinmende/glash)
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/L3L3F09W2)

</div>

## Use case
This crate is a slim, single dependency crate that basically extends glam. There are two main usecases:
1. [Rust-Gpu](https://github.com/EmbarkStudios/rust-gpu) usage: You are writing shader code and need a simple collision detection mechanism.
2. Simple collision detection: You are writing a *simple* application that needs light collision detection.

If you want to find collisions in a complex scene or need more sophisticated primitives, this is **not** the right crate. Have a look at alternatives below.

The crate only provides collision information and detection capabilities. Physics etc. are not in the scope. 


## Philosophy

Since the whole crates aim is to be *simple* all structs are completely public. If useful they contain `From` implementations for tuple of their internal type 
and multiple constructors. Apart from that the heart of this crate is the `Intersect` trait that, similar to other operations implements all primitive intersection
methods.

## Usage

Simple Box-Box detection.
``` rust
let a = Aabb::new(Vec3::ZERO, Vec3::ONE);
let b = Aabb::new(Vec3::splat(0.5), Vec3::splat(1.5));

assert(a.intersects(&b));

```
# Alternatives
- [collision](https://crates.io/crates/collision): Easy to interface with via `mint`.
- [ncollide3d](https://crates.io/crates/ncollide3d)
- [parry](https://crates.io/crates/parry2d)

# License
Licensed under either of [Apache License, Version 2.0](LICENSE-APACHE)
or [MIT license](LICENSE-MIT)
at your option.
