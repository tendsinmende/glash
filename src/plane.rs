use glam::{Vec3, Vec4, Vec4Swizzles};

use crate::{Intersect, Relation, Sphere};

/// A 3-dimensional plane formed from the equation: `A*x + B*y + C*z - D = 0`.
///
/// # Fields
///
/// - `n`: a unit vector representing the normal of the plane where:
///   - `n.x`: corresponds to `A` in the plane equation
///   - `n.y`: corresponds to `B` in the plane equation
///   - `n.z`: corresponds to `C` in the plane equation
/// - `d`: the distance value, corresponding to `D` in the plane equation.
///
/// The normal `n` points "insie" the plane. Therefore any point `p` with `p.dot(n) > d` is defined as "outside" the plane.
/// Note that is different to the other common version: `A*x + B*y + C*z + D = 0`. However, this implementation follows
/// the book "real-time-collision-detection by Christer Ericson which uses that definition.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Plane {
    pub normal: Vec3,
    pub dist: f32,
}

impl Plane {
    pub fn new(normal: Vec3, dist: f32) -> Self {
        Plane { normal, dist }
    }

    ///Creates the plane from three points on that plane.
    /// If two points are the same this will panic or might fail silently, see [glam's Vec3::normalize](Vec3::normalize).
    pub fn from_points(a: Vec3, b: Vec3, c: Vec3) -> Self {
        let v0 = b - a;
        let v1 = c - a;

        let n = v0.cross(v1);
        //Note we do not test for unormalizable
        let n = n.normalize();
        let d = (-a).dot(n);
        Plane { normal: n, dist: d }
    }

    ///Creates the plane from a point on the plane, and the normal at that point
    pub fn from_point_normal(point: Vec3, normal: Vec3) -> Self {
        Plane {
            normal: normal.normalize(),
            dist: point.dot(normal),
        }
    }

    ///Relates the point to this plane. Based on the plane's normal a point is either inside, outside, or directly on the plane.
    pub fn relate_point(&self, p: Vec3) -> Relation {
        let dist = p.dot(self.normal);
        if dist > self.dist {
            Relation::In
        } else if dist < self.dist {
            Relation::Out
        } else {
            Relation::Cross
        }
    }

    ///Returns the closest point on the plane for `point`. Or, in other words the the point with the smallest distance of `point` to the plane
    pub fn closest_point(&self, point: Vec3) -> Vec3 {
        let t = (self.normal.dot(point) - self.dist) / self.normal.dot(self.normal);
        point - t * self.normal
    }

    /// Normalise a plane.
    pub fn normalize(&self) -> Option<Self> {
        if self.normal.abs_diff_eq(Vec3::ZERO, f32::EPSILON) {
            None
        } else {
            let denom = 1.0 / self.normal.length();
            Some(Plane::new(self.normal * denom, self.dist * denom))
        }
    }
}

impl From<(Vec3, f32)> for Plane {
    fn from(value: (Vec3, f32)) -> Self {
        Self::new(value.0, value.1)
    }
}

impl From<(Vec3, Vec3, Vec3)> for Plane {
    fn from(value: (Vec3, Vec3, Vec3)) -> Self {
        Self::from_points(value.0, value.1, value.2)
    }
}

impl From<Vec4> for Plane {
    ///Creates the plane assuming that `value.xyz` are the planes normal, and `value.w` is the distance.
    fn from(value: Vec4) -> Self {
        Plane {
            normal: value.xyz(),
            dist: -value.w,
        }
    }
}

///Computes the point at which three planes intersect, if there is such a point.
///Taken from Real-Time Collision Detection: p. 213/214
impl Intersect<(&Plane, &Plane)> for Plane {
    type Intersection = Option<Vec3>;
    fn intersect(&self, other: &(&Plane, &Plane)) -> Self::Intersection {
        let p1 = self.normalize().unwrap();
        let p2 = other.0.normalize().unwrap();
        let p3 = other.1.normalize().unwrap();
        let u = p2.normal.cross(p3.normal);
        let denom = p1.normal.dot(u);
        //Some are parallel

        if denom.abs() < f32::EPSILON {
            return None;
        }

        let p = (p1.dist * u + p1.normal.cross(p3.dist * p2.normal - p2.dist * p3.normal)) / denom;
        Some(p)
    }
}

impl Intersect<Sphere> for Plane {
    type Intersection = bool;
    fn intersect(&self, other: &Sphere) -> Self::Intersection {
        let dist = other.center.dot(self.normal) - self.dist;
        dist.abs() <= other.radius
    }
}

#[cfg(test)]
mod test {
    use glam::Vec3;

    use crate::{Intersect, Plane, Relation};

    #[test]
    fn plane_point_in() {
        let plane = Plane::new(Vec3::Y, 0.0);
        //Below plane
        let point = Vec3::new(1.0, 1.0, -1.0);

        assert!(
            plane.relate_point(point) == Relation::In,
            "expected Relation::In, got {:?}",
            plane.relate_point(point)
        );
    }

    #[test]
    fn plane_point_cross() {
        let plane = Plane::new(Vec3::Y, 0.0);
        //Below plane
        let point = Vec3::new(1.0, 0.0, -1.0);

        assert!(
            plane.relate_point(point) == Relation::Cross,
            "expected Relation::Cross, got {:?}",
            plane.relate_point(point)
        );
    }

    #[test]
    fn plane_point_out() {
        let plane = Plane::new(Vec3::Y, 0.0);
        //Below plane
        let point = Vec3::new(1.0, -1.0, -1.0);

        assert!(
            plane.relate_point(point) == Relation::Out,
            "expected Relation::Out, got {:?}",
            plane.relate_point(point)
        );
    }

    #[test]
    fn three_plane_intersection() {
        let a = Plane::new(Vec3::X, 0.0);
        let b = Plane::new(Vec3::Y, 0.0);
        let c = Plane::new(Vec3::Z, 0.0);

        assert!(a.intersect(&(&b, &c)).is_some())
    }

    #[test]
    fn three_plane_no_intersection() {
        let a = Plane::new(Vec3::X, 0.0);
        let b = Plane::new(Vec3::Y, 0.0);
        let c = Plane::new(Vec3::Y, 1.0);

        assert!(a.intersect(&(&b, &c)).is_none())
    }
}
