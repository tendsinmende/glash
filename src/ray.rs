use glam::{Mat4, Vec3};

pub struct Ray {
    pub origin: Vec3,
    ///Needs to be normalized!
    pub direction: Vec3,
}

impl Ray {
    pub fn on_ray(&self, t: f32) -> Vec3 {
        self.origin + (self.direction * t)
    }

    ///Transforms the ray into a new coordinate system
    pub fn transform(&self, transform: Mat4) -> Ray {
        Ray {
            origin: transform.transform_point3(self.origin),
            direction: transform.transform_vector3(self.direction),
        }
    }
}
