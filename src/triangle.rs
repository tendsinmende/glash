use glam::Vec3;

use crate::{Intersect, Ray, Sphere};

///Triangle made from three vertices.
pub struct Triangle {
    pub v0: Vec3,
    pub v1: Vec3,
    pub v2: Vec3,
}

impl Triangle {
    //see realtime collision 5.1.5
    pub fn closest_point(&self, point: Vec3) -> Vec3 {
        let ab = self.v1 - self.v0;
        let ac = self.v2 - self.v0;
        let bc = self.v2 - self.v1;

        let snom = (point - self.v0).dot(ab);
        let sdenom = (point - self.v1).dot(self.v0 - self.v1);

        let tnom = (point - self.v0).dot(ac);
        let tdenom = (point - self.v2).dot(self.v0 - self.v2);

        if snom <= 0.0 && tnom <= 0.0 {
            return self.v0;
        }

        let unom = (point - self.v1).dot(bc);
        let udenom = (point - self.v2).dot(self.v1 - self.v2);

        if sdenom <= 0.0 && unom <= 0.0 {
            return self.v1;
        }
        if tdenom <= 0.0 && udenom <= 0.0 {
            return self.v2;
        }

        let n = (self.v1 - self.v0).cross(self.v2 - self.v0);
        let vc = n.dot((self.v0 - point).cross(self.v1 - point));
        if vc <= 0.0 && snom >= 0.0 && sdenom >= 0.0 {
            return self.v0 + snom / (snom + sdenom) * ab;
        }

        let va = n.dot((self.v1 - point).cross(self.v2 - point));
        if va <= 0.0 && unom >= 0.0 && udenom >= 0.0 {
            return self.v1 + unom / (unom + udenom) * bc;
        }

        let vb = n.dot((self.v2 - point).cross(self.v0 - point));
        if vb <= 0.0 && tnom >= 0.0 && tdenom >= 0.0 {
            return self.v0 + tnom / (tnom + tdenom) * ac;
        }

        let u = va / (va + vb + vc);
        let v = vb / (va + vb + vc);
        let w = 1.0 - u - v;
        u * self.v0 + v * self.v1 + w * self.v2
    }
}

impl Intersect<Ray> for Triangle {
    type Intersection = Option<f32>;
    fn intersect(&self, other: &Ray) -> Self::Intersection {
        //Typical Moeller-Trumbore intersection
        const EPS: f32 = 0.000001;
        let edge1 = self.v1 - self.v0;
        let edge2 = self.v2 - self.v0;
        let h = other.direction.cross(edge2);
        let a = edge1.dot(h);

        //Is parallel?
        if a > -EPS && a < EPS {
            return None;
        }

        let f = 1.0 / a;
        let s = other.origin - self.v0;
        let u = f * s.dot(h);

        //Out of bound?
        if u < 0.0 || u > 1.0 {
            return None;
        }

        let q = s.cross(edge1);
        let v = f * other.direction.dot(q);

        //Out of bound again
        if v < 0.0 || u + v > 1.0 {
            return None;
        }

        let t = f * edge2.dot(q);
        if t > EPS {
            Some(t)
        } else {
            None
        }
    }
}

impl Intersect<Sphere> for Triangle {
    type Intersection = bool;
    fn intersect(&self, other: &Sphere) -> Self::Intersection {
        other.intersect(self)
    }
}

#[cfg(test)]
mod test {
    use glam::Vec3;

    use crate::{Intersect, Ray, Triangle};

    #[test]
    fn ray_triangle_intersecting() {
        let tri = Triangle {
            v0: Vec3::ONE,
            v1: Vec3::NEG_ONE,
            v2: Vec3::new(1.0, 0.0, -1.0),
        };

        let ray = Ray {
            origin: Vec3::new(0.5, 2.0, -0.5),
            direction: Vec3::new(0.0, -1.0, 0.0),
        };

        assert!(tri.intersect(&ray).is_some());
    }
    #[test]
    fn ray_triangle_no_intersecting() {
        let tri = Triangle {
            v0: Vec3::ONE,
            v1: Vec3::NEG_ONE,
            v2: Vec3::new(1.0, 0.0, -1.0),
        };

        let ray = Ray {
            origin: Vec3::ONE * 2.0,
            direction: Vec3::new(0.0, -1.0, 0.0),
        };

        assert!(tri.intersect(&ray).is_none());
    }
}
