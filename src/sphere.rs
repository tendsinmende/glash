use glam::Vec3;

use crate::{Aabb, Intersect, Triangle};

pub struct Sphere {
    pub center: Vec3,
    pub radius: f32,
}

impl Intersect<Aabb> for Sphere {
    type Intersection = bool;
    fn intersect(&self, other: &Aabb) -> Self::Intersection {
        let p = other.closest_point(self.center);
        let v = p - self.center;

        v.dot(v) <= self.radius * self.radius
    }
}

impl Intersect<Sphere> for Sphere {
    type Intersection = bool;
    fn intersect(&self, other: &Sphere) -> Self::Intersection {
        let d = self.center - other.center;
        let dist2 = d.dot(d);
        let radius_sum = self.radius + other.radius;
        dist2 <= radius_sum * radius_sum
    }
}

impl Intersect<Triangle> for Sphere {
    type Intersection = bool;
    fn intersect(&self, other: &Triangle) -> Self::Intersection {
        let p = other.closest_point(self.center);
        let v = p - self.center;
        v.dot(v) <= self.radius * self.radius
    }
}

#[cfg(test)]
mod test {
    use glam::Vec3;

    use crate::{Intersect, Sphere, Triangle};

    #[test]
    fn sphere_triangle() {
        let sphere = Sphere {
            center: Vec3::ZERO,
            radius: 1.0,
        };
        let triangle = Triangle {
            v0: Vec3::new(-0.5, 0.0, -0.5),
            v1: Vec3::new(0.0, 0.0, 0.5),
            v2: Vec3::new(0.5, 0.0, -0.5),
        };

        assert!(sphere.intersect(&triangle))
    }

    #[test]
    fn sphere_not_triangle() {
        let sphere = Sphere {
            center: Vec3::new(0.0, 1.0, 0.0),
            radius: 0.5,
        };
        let triangle = Triangle {
            v0: Vec3::new(-0.5, 0.0, -0.5),
            v1: Vec3::new(0.0, 0.0, 0.5),
            v2: Vec3::new(0.5, 0.0, -0.5),
        };

        assert!(!sphere.intersect(&triangle))
    }

    #[test]
    fn sphere_dist() {
        let sphere = Sphere {
            center: Vec3::new(0.0, 1.0, 0.0),
            radius: 0.5,
        };
        let triangle = Triangle {
            v0: Vec3::new(-0.5, 0.0, -0.5),
            v1: Vec3::new(0.0, 0.0, 0.5),
            v2: Vec3::new(0.5, 0.0, -0.5),
        };

        let dist = triangle.closest_point(Vec3::new(0.0, 1.0, 0.0));
        assert!(Vec3::ZERO == dist, "dist was {dist}")
    }
}
