use glam::{Mat4, Vec3};

use crate::{Intersect, Plane, Ray, Relation};

///AxisAlignedBoundingBox. Min and Max contain the minimum and maximum coordinates on each
/// axis respectively.
///
/// If you construct the struct your self make sure that holds true, or test via [Aabb::is_valid()]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Aabb {
    pub min: Vec3,
    pub max: Vec3,
}

impl Aabb {
    ///AABB containing everything from -inf to inf.
    pub const ALL: Self = Aabb {
        min: Vec3::splat(f32::NEG_INFINITY),
        max: Vec3::splat(f32::INFINITY),
    };

    ///New from two points that do not necessarily have to be min/max. If you are sure about
    /// that use [Self::min_max]
    pub fn new(p0: Vec3, p1: Vec3) -> Self {
        let min = p0.min(p1);
        let max = p0.max(p1);
        Self::min_max(min, max)
    }

    ///Creates, assuming min and max are actually the min and max value. In debug this fails if it is not the case.
    pub fn min_max(min: Vec3, max: Vec3) -> Self {
        debug_assert!(min.x <= max.x && min.y <= max.y && min.z <= max.z);
        Aabb { min, max }
    }

    ///Returns the AABB that contains all `points`.
    ///
    /// returns Self::ALL if `points` is empty.
    pub fn from_points(points: &[Vec3]) -> Self {
        if points.len() == 0 {
            return Self::ALL;
        }
        let mut aabb = Self::min_max(points[0], points[0]);
        for p in points {
            aabb.min = aabb.min.min(*p);
            aabb.max = aabb.max.max(*p);
        }

        aabb
    }

    ///Returns the 8 corner points of this box
    pub fn corner_points(&self) -> [Vec3; 8] {
        [
            Vec3::new(self.min.x, self.min.y, self.min.z),
            Vec3::new(self.min.x, self.min.y, self.max.z),
            Vec3::new(self.min.x, self.max.y, self.min.z),
            Vec3::new(self.min.x, self.max.y, self.max.z),
            Vec3::new(self.max.x, self.min.y, self.min.z),
            Vec3::new(self.max.x, self.min.y, self.max.z),
            Vec3::new(self.max.x, self.max.y, self.min.z),
            Vec3::new(self.max.x, self.max.y, self.max.z),
        ]
    }

    ///Returns the edges of the bound as pairs of corner points.
    pub fn edges(&self) -> [(Vec3, Vec3); 12] {
        let corners = self.corner_points();
        [
            //lower plane on y up system
            (corners[0], corners[1]),
            (corners[0], corners[2]),
            (corners[1], corners[3]),
            (corners[2], corners[3]),
            //top plane on y up
            (corners[4], corners[5]),
            (corners[4], corners[6]),
            (corners[6], corners[7]),
            (corners[5], corners[7]),
            //sides
            (corners[0], corners[4]),
            (corners[1], corners[5]),
            (corners[2], corners[6]),
            (corners[3], corners[7]),
        ]
    }

    ///Returns true if this is a valid AABB.
    pub fn is_valid(&self) -> bool {
        self.min.x <= self.max.x && self.min.y <= self.max.y && self.min.z <= self.max.z
    }

    ///Extent of each axis of this aabb from `min` to `max`.
    pub fn extent(&self) -> Vec3 {
        debug_assert!(self.is_valid());
        self.max - self.min
    }

    pub fn half_extent(&self) -> Vec3 {
        self.extent() / 2.0
    }

    ///Center point of aabb
    pub fn center(&self) -> Vec3 {
        self.min + self.half_extent()
    }

    pub fn relate_plane(&self, plane: &Plane) -> Relation {
        let corners = self.corner_points();
        let first = plane.relate_point(corners[0]);
        for p in corners[1..].iter() {
            if plane.relate_point(*p) != first {
                return Relation::Cross;
            }
        }
        first
    }

    ///Returns the transformed AABB. This will calculate the tightest AABB that contains the transformed box.
    pub fn transformed(&self, transform: Mat4) -> Self {
        let (min, max) = self
            .corner_points()
            .iter()
            .map(|p| transform.transform_point3(*p))
            .fold(
                (Vec3::splat(f32::INFINITY), Vec3::splat(f32::NEG_INFINITY)),
                |(min, max), p| (min.min(p), max.max(p)),
            );
        Aabb::new(min, max)
    }

    ///Returns the closest point on the AABB relative to `point`.
    pub fn closest_point(&self, point: Vec3) -> Vec3 {
        let mut q = Vec3::ZERO;
        for i in 0..3 {
            let mut v = point[i];
            if v < self.min[i] {
                v = self.min[i];
            }
            if v > self.max[i] {
                v = self.max[i];
            }
            q[i] = v;
        }
        q
    }
}

impl From<(Vec3, Vec3)> for Aabb {
    fn from(value: (Vec3, Vec3)) -> Self {
        Self::new(value.0, value.1)
    }
}

impl Intersect<Aabb> for Aabb {
    type Intersection = bool;
    fn intersect(&self, other: &Aabb) -> Self::Intersection {
        let (a0, a1) = (self.min, self.max);
        let (b0, b1) = (other.min, other.max);

        a1.x > b0.x && a0.x < b1.x && a1.y > b0.y && a0.y < b1.y && a1.z > b0.z && a0.z < b1.z
    }
}

impl Intersect<Plane> for Aabb {
    type Intersection = Relation;
    fn intersect(&self, other: &Plane) -> Self::Intersection {
        self.relate_plane(other)
    }
}

impl Intersect<Vec3> for Aabb {
    type Intersection = bool;
    fn intersect(&self, p: &Vec3) -> Self::Intersection {
        self.min.x <= p.x
            && p.x < self.max.x
            && self.min.y <= p.y
            && p.y < self.max.y
            && self.min.z <= p.z
            && p.z < self.max.z
    }
}

impl Intersect<Ray> for Aabb {
    type Intersection = (Option<f32>, Option<f32>);
    fn intersect(&self, other: &Ray) -> Self::Intersection {
        let inv_dir = Vec3::ONE / other.direction;

        let mut t1 = (self.min.x - other.origin.x) * inv_dir.x;
        let mut t2 = (self.max.x - other.origin.x) * inv_dir.x;

        let mut tmin = t1.min(t2);
        let mut tmax = t1.max(t2);

        for i in 1..3 {
            t1 = (self.min[i] - other.origin[i]) * inv_dir[i];
            t2 = (self.max[i] - other.origin[i]) * inv_dir[i];

            tmin = tmin.max(t1.min(t2));
            tmax = tmax.min(t1.max(t2));
        }

        if tmax >= tmin {
            (
                if tmin >= 0.0 { Some(tmin) } else { None },
                if tmax >= 0.0 { Some(tmax) } else { None },
            )
        } else {
            (None, None)
        }
    }
}

#[cfg(test)]
mod test {
    use glam::Vec3;

    use crate::{Aabb, Intersect, Ray};

    #[test]
    fn aabb_aabb_intersecting() {
        let one = Aabb::min_max(Vec3::splat(-1.0), Vec3::splat(0.5));
        let two = Aabb::min_max(Vec3::splat(0.0), Vec3::splat(1.0));

        assert!(one.intersect(&two));
    }

    #[test]
    fn aabb_aabb_not_intersecting() {
        let one = Aabb::min_max(Vec3::splat(-1.0), Vec3::splat(0.0));
        let two = Aabb::min_max(Vec3::splat(0.5), Vec3::splat(1.0));

        assert!(!one.intersect(&two));
    }

    #[test]
    fn aabb_ray_intersecting() {
        let aabb = Aabb::min_max(Vec3::splat(-0.5), Vec3::splat(0.5));
        let ray = Ray {
            origin: Vec3::splat(2.0),
            direction: Vec3::NEG_ONE,
        };

        assert!(aabb.intersect(&ray).0.is_some());
        assert!(aabb.intersect(&ray).1.is_some());
    }

    #[test]
    fn aabb_ray_not_intersecting() {
        let aabb = Aabb::min_max(Vec3::splat(-0.5), Vec3::splat(0.5));
        let ray = Ray {
            origin: Vec3::splat(2.0),
            direction: Vec3::ONE,
        };

        assert!(aabb.intersect(&ray).0.is_none());
        assert!(aabb.intersect(&ray).1.is_none());
    }

    #[test]
    fn aabb_point_non_intersecting() {
        let point = Vec3::splat(2.0);
        let point1 = Vec3::new(-1.0, 1.0, 2.0);
        let aabb = Aabb::min_max(Vec3::splat(-0.5), Vec3::splat(0.5));

        assert!(!aabb.intersect(&point));
        assert!(!aabb.intersect(&point1));
    }

    #[test]
    fn aabb_point_intersecting() {
        let point = Vec3::splat(0.25);
        let point1 = Vec3::new(-0.25, 0.0, 0.25);
        let aabb = Aabb::min_max(Vec3::splat(-0.5), Vec3::splat(0.5));

        assert!(aabb.intersect(&point));
        assert!(aabb.intersect(&point1));
    }
}
