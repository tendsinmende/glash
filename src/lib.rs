mod frustum;
pub use frustum::Frustum;
mod plane;
pub use plane::Plane;
mod aabb;
pub use aabb::Aabb;
mod triangle;
pub use triangle::Triangle;
mod ray;
pub use ray::Ray;
mod sphere;
///Glam version used by this crate.
pub use glam;
pub use sphere::Sphere;

pub trait Intersect<Rhs> {
    type Intersection;
    fn intersect(&self, other: &Rhs) -> Self::Intersection;
}

#[derive(Clone, Copy, Eq, Hash, Ord, PartialOrd, PartialEq, Debug)]
pub enum Relation {
    ///Compleatly inside
    In,
    ///Crossing the bound
    Cross,
    ///Compleatly outside
    Out,
}
