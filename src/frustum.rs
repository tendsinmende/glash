use glam::{Mat4, Vec3};

use crate::{Aabb, Intersect, Plane, Relation};

///Camera frustum made from planes in space.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Frustum {
    pub left: Plane,
    pub right: Plane,
    pub top: Plane,
    pub bottom: Plane,
    pub near: Plane,
    pub far: Plane,
}

impl Frustum {
    /// Extract frustum planes from a projection matrix.
    ///
    /// Usually you want to supply your View-Projection matrix and do frustum culling for worldspace AABBs of your objects.
    ///
    /// Might return none if the matrix makes no sense.
    pub fn from_matrix4(mat: Mat4) -> Option<Self> {
        let left = match Plane::from(mat.row(3) + mat.row(0)).normalize() {
            Some(p) => p,
            None => return None,
        };

        let right = match Plane::from(mat.row(3) - mat.row(0)).normalize() {
            Some(p) => p,
            None => return None,
        };

        let bottom = match Plane::from(mat.row(3) + mat.row(1)).normalize() {
            Some(p) => p,
            None => return None,
        };
        let top = match Plane::from(mat.row(3) - mat.row(1)).normalize() {
            Some(p) => p,
            None => return None,
        };

        let near = match Plane::from(mat.row(3) + mat.row(2)).normalize() {
            Some(p) => p,
            None => return None,
        };

        let far = match Plane::from(mat.row(3) - mat.row(2)).normalize() {
            Some(p) => p,
            None => return None,
        };
        Some(Frustum {
            left,
            right,
            bottom,
            top,
            near,
            far,
        })
    }

    ///Returns the frustum corner points. Might return None if the frustum contains an invalid set of planes.
    pub fn corner_points(&self) -> Option<[Vec3; 8]> {
        //We calculate corner points by building intersection points of all
        // incident planes of any corner point.
        // FIXME: There must be a faster way.
        let near_left_top = self.near.intersect(&(&self.left, &self.top))?;
        let near_right_top = self.near.intersect(&(&self.right, &self.top))?;
        let near_left_bottom = self.near.intersect(&(&self.left, &self.bottom))?;
        let near_right_bottom = self.near.intersect(&(&self.right, &self.bottom))?;

        let far_left_top = self.far.intersect(&(&self.left, &self.top))?;
        let far_right_top = self.far.intersect(&(&self.right, &self.top))?;
        let far_left_bottom = self.far.intersect(&(&self.left, &self.bottom))?;
        let far_right_bottom = self.far.intersect(&(&self.right, &self.bottom))?;

        Some([
            near_right_bottom,
            near_right_top,
            near_left_bottom,
            near_left_top,
            far_right_bottom,
            far_right_top,
            far_left_bottom,
            far_left_top,
        ])
    }

    pub fn edges(&self) -> Option<[(Vec3, Vec3); 12]> {
        let corners = self.corner_points()?;

        Some([
            (corners[0], corners[1]),
            (corners[0], corners[2]),
            (corners[2], corners[3]),
            (corners[1], corners[3]),
            (corners[4], corners[6]),
            (corners[4], corners[5]),
            (corners[6], corners[7]),
            (corners[5], corners[7]),
            (corners[0], corners[4]),
            (corners[2], corners[6]),
            (corners[3], corners[7]),
            (corners[1], corners[5]),
        ])
    }
}

impl Intersect<Aabb> for Frustum {
    type Intersection = Relation;
    fn intersect(&self, other: &Aabb) -> Relation {
        //We compare all of our planes to all planes of the aabb.
        // We then take the maximum relation of all.
        //
        // This means if any of the planes are "out", the whole bound is out,
        // if the max is cross, its crossing, otherwise we are inside.
        let Frustum {
            left,
            right,
            top,
            bottom,
            near,
            far,
        } = &self;

        let mut relation = Relation::In;

        for plane in [left, right, top, bottom, near, far] {
            relation = relation.max(other.relate_plane(plane));
        }

        relation
    }
}

#[cfg(test)]
mod test {
    use std::f32::consts::PI;

    use glam::{Mat4, Vec3};

    use crate::Frustum;

    #[test]
    fn frustum_create() {
        let projection = Mat4::perspective_infinite_lh(PI, 1.0, 0.1);
        let view = Mat4::look_at_lh(Vec3::splat(10.0), Vec3::ZERO, Vec3::Y);
        let fr = Frustum::from_matrix4(view * projection);

        assert!(fr.is_some())
    }
}
